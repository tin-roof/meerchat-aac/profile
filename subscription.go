package profile

import (
	"context"
	"errors"
	"time"

	"github.com/google/uuid"
	"gitlab.com/tin-roof/meerchat-aac/database"
	"gitlab.com/tin-roof/meerchat-aac/logger"
)

var (
	// ErrNoSubscription is returned when a user does not have a subscription
	ErrNoSubscription = errors.New("no subscription")
)

type Subscription struct {
	Active          bool       `json:"active"`
	CreatedAt       time.Time  `json:"createdAt,omitempty"`
	DashboardAccess bool       `json:"dashboardAccess"`
	ExpiresOn       time.Time  `json:"expiresOn,omitempty"`
	FamilyID        *uuid.UUID `json:"familyId,omitempty"`
	ID              *uuid.UUID `json:"id,omitempty"`
	Paid            bool       `json:"paid"`
	TotalSeats      int        `json:"totalSeats"`
	UpdatedAt       time.Time  `json:"updated_at,omitempty"`
	UsedSeats       int        `json:"usedSeats"`
	Renew           bool       `json:"renew"`
}

// DeleteSubscription delete a subscription
func (p *Profile) DeleteSubscription(ctx context.Context, db database.DB) error {
	ctx, log := logger.New(ctx, "profile.Profile.DeleteSubscription", logger.WithField("profileID", p.ID))

	log.Info(ctx, "deleting subscription")

	// get the subscription id
	sub, err := p.GetSubscription(ctx, db)
	if err != nil {
		return err
	}

	// set the subscription to not renew
	sub.Renew = false

	return sub.Save(ctx, db)
}

// GetSubscription finds the subscription for the family
func (p *Profile) GetSubscription(ctx context.Context, db database.DB) (*Subscription, error) {
	ctx, log := logger.New(ctx, "profile.Profile.Subscription", logger.WithField("profileID", p.ID))

	log.Info(ctx, "looking up subscription")

	// if the user doesn't have a subscription, double check that it just wasn't looked up yet
	if !p.HasSubscription() {
		_, err := p.GetSubscriptionID(ctx, db)
		if err != nil {
			return nil, err
		}
		if !p.HasSubscription() {
			return nil, ErrNoSubscription
		}
	}

	// lookup the subscription details
	var s Subscription
	err := db.Conn.QueryRow(
		ctx,
		`SELECT 
			s."id",
			s."created_at",
			s."updated_at",
			s."expires_on",
			s."family",
			s."seats" as "total_seats",
			COUNT(DISTINCT u."id") as "used_seats",
			s."dashboard_access",
			s."paid",
			s."active",
			s."renew"
		FROM 
			"subscription" s,
			"user" u
		WHERE 
			u."subscription" = s."id" AND
			s."id" = $1
		GROUP BY 1
		LIMIT 1;`,
		p.SubscriptionID,
	).
		Scan(
			&s.ID,
			&s.CreatedAt,
			&s.UpdatedAt,
			&s.ExpiresOn,
			&s.FamilyID,
			&s.TotalSeats,
			&s.UsedSeats,
			&s.DashboardAccess,
			&s.Paid,
			&s.Active,
			&s.Renew,
		)
	if err != nil {
		log.Info(ctx, "query failed for looking up subscription")
		return nil, err
	}

	return &s, nil
}

// GetSubscriptionID for a profile
func (p *Profile) GetSubscriptionID(ctx context.Context, db database.DB) (*Profile, error) {
	ctx, log := logger.New(ctx, "profile.Profile.GeSubscriptionID", logger.WithField("profileID", p.ID))

	log.Info(ctx, "looking up subscription id for user")

	err := db.Conn.QueryRow(
		ctx,
		`SELECT "subscription" FROM "user" WHERE "id" = $1;`,
		p.ID,
	).Scan(&p.SubscriptionID)
	if err != nil {
		log.Info(ctx, "query failed for looking up user subscription id")
		return p, err
	}

	return p, nil
}

// NewSubscription creates a new subscription for a profile
func (p *Profile) NewSubscription(ctx context.Context, db database.DB, seats int, dashboardAccess bool) (*Subscription, error) {
	ctx, log := logger.New(ctx, "profile.Profile.NewSubscription", logger.WithField("profileID", p.ID))

	log.Info(ctx, "creating a new subscription for a user")

	// get the family details for the user, this also gets any members so we can easily
	// update each one with the new subscription id (there will probably only be 1 anyways
	// because you can't add family without a subscription)
	family, err := p.GetFamily(ctx, db)
	if err != nil {
		log.Info(ctx, "query failed for looking up family details")
		return nil, err
	}

	// start a transaction
	if err := db.Transaction(ctx); err != nil {
		log.Info(ctx, "failed to start a transaction for creating a new subscription")
		return nil, err
	}
	// roll back the transaction if there is an error
	defer db.TX.Rollback(ctx)

	// create a new subscription
	var s Subscription
	err = db.TX.Conn.QueryRow(
		ctx,
		`INSERT INTO "subscription" ("expires_on", "family", "seats", "dashboard_access", "paid", "active") VALUES ($1, $2, $3, $4, $5, $6) RETURNING "id";`,
		time.Now().AddDate(1, 0, 0), // expires in 1 year
		family.ID,
		seats,
		dashboardAccess,
		true,
		true,
	).Scan(&s.ID)
	if err != nil {
		log.Info(ctx, "query failed for creating a new subscription")
		return nil, err
	}

	// @NOTE: no longer want to add the subscription to all family members because
	// the subscription is for the dashboard and is seat related not whole family group
	// so each user will need to be toggled individually by an admin
	// @TODO: remove this after testings
	// update all family members with the new subscription id
	// for _, member := range family.Members {
	// 	_, err := db.TX.Conn.Exec(
	// 		ctx,
	// 		`UPDATE "user" SET "subscription" = $1 WHERE "id" = $2;`,
	// 		s.ID,
	// 		member.Profile.ID,
	// 	)
	// 	if err != nil {
	// 		log.Info(ctx, "query failed for updating a family member with the new subscription id")
	// 		return nil, err
	// 	}
	// }

	// save all the changes
	if err := db.TX.Commit(ctx); err != nil {
		log.Info(ctx, "failed to commit the transaction for creating a new subscription")
		return nil, err
	}

	log.Info(ctx, "successfully created a new subscription for the family")
	return &s, nil
}

// RemoveSubscription from a profile
func (p *Profile) RemoveSubscription(ctx context.Context, db database.DB) error {
	ctx, log := logger.New(ctx, "profile.Profile.RemoveSubscription", logger.WithField("profileID", p.ID))

	log.Info(ctx, "removing the subscription from a user record")

	p.SubscriptionID = nil

	// remove the subscription id from a profile
	if _, err := db.Conn.Exec(ctx, `UPDATE "user" SET "subscription" = NULL WHERE "id" = $1;`, p.ID); err != nil {
		return err
	}

	return nil
}

// Save saves any changes to the subscription
func (s *Subscription) Save(ctx context.Context, db database.DB) error {
	ctx, log := logger.New(ctx, "profile.Subscription.Save", logger.WithField("subscriptionID", s.ID))

	log.Info(ctx, "updating the subscription for a user")

	// update the subscription
	if _, err := db.Conn.Exec(
		ctx,
		`UPDATE "subscription" 
		SET 
			"updated_at" = $1, 
			"expires_on" = $2, 
			"seats" = $3, 
			"dashboard_access" = $4, 
			"paid" = $5, 
			"active" = $6,
			"renew" = $7
		WHERE "id" = $8;`,
		time.Now(),
		s.ExpiresOn,
		s.TotalSeats,
		s.DashboardAccess,
		s.Paid,
		s.Active,
		s.Renew,
		s.ID,
	); err != nil {
		log.Info(ctx, "query failed for updating the subscription")
		return err
	}

	return nil
}

// Has subscription returns true if there is a valid subscription id for the user
func (p *Profile) HasSubscription() bool {
	return p.SubscriptionID != nil && *p.SubscriptionID != uuid.Nil
}
