package profile

import (
	"context"
	_ "embed"
	"time"

	"github.com/google/uuid"
	"gitlab.com/tin-roof/meerchat-aac/database"
	"gitlab.com/tin-roof/meerchat-aac/logger"
)

type Settings struct {
	Analytics bool           `db:"analytics" json:"analytics"`
	Speech    SpeechSettings `db:"speech" json:"speech"`
	Theme     ThemeSettings  `db:"theme" json:"theme"`
}

type SpeechSettings struct {
	Pitch float64 `db:"pitch" json:"pitch"`
	Speed float64 `db:"speed" json:"speed"`
	Voice string  `db:"voice" json:"voice"`
}

type ThemeSettings struct {
	BackgroundColor string `db:"backgroundColor" json:"backgroundColor"`
	BackgroundImage string `db:"backgroundImage" json:"backgroundImage"`
	Theme           string `db:"theme" json:"theme"`
}

// DeleteSettings deletes the users application settings
func (p *Profile) DeleteSettings(ctx context.Context, db database.DB) error {
	ctx, log := logger.New(ctx, "profile.Profile.DeleteSettings", logger.WithField("profileID", p.ID))
	log.Info(ctx, "deleting settings for profile")

	// delete the settings
	_, err := db.Conn.Exec(
		ctx,
		`DELETE FROM "settings" WHERE "user" = $1;`,
		p.ID,
	)
	if err != nil {
		log.Info(ctx, "error deleting settings for profile")
		return err
	}

	return nil
}

// GetSettings finds the users application settings
func (p *Profile) GetSettings(ctx context.Context, db database.DB) (*Settings, error) {
	ctx, log := logger.New(ctx, "profile.Profile.GetSettings", logger.WithField("profileID", p.ID))
	log.Info(ctx, "getting settings for profile")

	// look up the settings
	var settings Settings
	err := db.Conn.QueryRow(
		ctx,
		`SELECT "settings" FROM "settings" WHERE "user" = $1;`,
		p.ID,
	).Scan(&settings)
	if err != nil {
		log.Info(ctx, "error getting settings for profile")
		return nil, err
	}

	return &settings, nil
}

// SaveSettings saves the users application settings
func (p *Profile) SaveSettings(ctx context.Context, db database.DB, settings Settings) error {
	ctx, log := logger.New(ctx, "profile.Profile.SaveSettings", logger.WithField("profileID", p.ID))
	log.Info(ctx, "save settings for profile")

	var id uuid.UUID
	err := db.Conn.QueryRow(
		ctx,
		`INSERT INTO "settings" ("user", "updated_at", "settings") VALUES ($1, $2, $3) ON CONFLICT ("user") DO UPDATE SET "updated_at" = $2, "settings" = $3 RETURNING "id";`,
		p.ID,
		time.Now(),
		settings,
	).Scan(&id)
	if err != nil {
		log.Info(ctx, "error saving settings for profile")
		return err
	}

	return nil
}
