package profile

import (
	"context"
	"time"

	"github.com/google/uuid"
	"gitlab.com/tin-roof/meerchat-aac/database"
	"gitlab.com/tin-roof/meerchat-aac/logger"
)

type Family struct {
	CreatedAt time.Time  `db:"created_at" json:"createdAt,omitempty"`
	ID        *uuid.UUID `db:"id" json:"id,omitempty"`
	UpdatedAt time.Time  `db:"updated_at" json:"updatedAt,omitempty"`

	Members []Membership `json:"members,omitempty"`
}

// Membership is a user's relationship to a family
type Membership struct {
	AcceptedInvite bool       `db:"accepted_invite" json:"acceptedInvite"`
	CreatedAt      *time.Time `db:"created_at" json:"createdAt,omitempty"`
	FamilyID       *uuid.UUID `db:"family_id" json:"familyId,omitempty"`
	ID             *uuid.UUID `db:"id" json:"id,omitempty"`
	InviteCode     *uuid.UUID `db:"invite_code" json:"inviteCode,omitempty"`
	IsAdmin        bool       `db:"is_admin" json:"isAdmin"`
	ProfileID      *uuid.UUID `db:"f_profile_id" json:"profileId,omitempty"`
	UpdatedAt      *time.Time `db:"updated_at" json:"updatedAt,omitempty"`

	// joined fields
	Profile   *Profile `json:"profile,omitempty"`
	Therapist *Profile `json:"therapist,omitempty"`
}

// GetFamily gets the family details for the profile
func (p *Profile) GetFamily(ctx context.Context, db database.DB) (*Family, error) {
	ctx, log := logger.New(ctx, "profile.Profile.GetFamily", logger.WithField("profileID", p.ID))
	log.Info(ctx, "getting family details")

	// lookup the family
	var family Family
	err := db.Conn.QueryRow(
		ctx,
		`SELECT f."id", f."created_at", f."updated_at" FROM "family" f, "family_members" fm  WHERE f."id" = fm."family" AND fm."user" = $1;`,
		p.ID,
	).Scan(&family.ID, &family.CreatedAt, &family.UpdatedAt)
	if err != nil {
		log.WithError(err).Info(ctx, "error running family member lookup query")
		return nil, err
	}

	// lookup the family members
	if err = family.GetMembers(ctx, db); err != nil {
		return nil, err
	}

	return &family, nil
}

// GetMembers gets the family member details for the profile
func (f *Family) GetMembers(ctx context.Context, db database.DB) error {
	ctx, log := logger.New(ctx, "profile.Family.GetMembers", logger.WithField("familyID", f.ID))

	// lookup the family members
	rows, err := db.Conn.Query(
		ctx,
		`SELECT 
			fm."accepted_invite",
			fm."created_at",
			fm."family" as family_id,
			fm."id",
			fm."invite_code",  
			fm."is_admin",
			fm."user" as f_profile_id,
			fm."updated_at",
			p."email" as profile_email,
			p."subscription" as profile_subscription_id,
			p."id" as profile_id, 
			p."image" as profile_image,
			p."name" as profile_name, 
			b."id" as home_board_id
		FROM 
			"user" p, 
			"family_members" fm,
			"page" b
		WHERE 
			fm."user" = p."id"
			AND b."user" = p."id"
			AND b."title" = 'home'
			AND fm."family" = $1;`,
		f.ID,
	)
	if err != nil {
		log.WithError(err).Info(ctx, "error running family member lookup query")
		return err
	}

	// build the family member list
	members := []Membership{}
	for rows.Next() {
		member := Membership{
			Profile: &Profile{},
		}
		if err := rows.Scan(
			&member.AcceptedInvite,
			&member.CreatedAt,
			&member.FamilyID,
			&member.ID,
			&member.InviteCode,
			&member.IsAdmin,
			&member.ProfileID,
			&member.UpdatedAt,
			&member.Profile.Email,
			&member.Profile.SubscriptionID,
			&member.Profile.ID,
			&member.Profile.Image,
			&member.Profile.Name,
			&member.Profile.HomeBoardID,
		); err != nil {
			log.WithError(err).Info(ctx, "error scanning family member lookup query row")
			continue
		}

		members = append(members, member)
	}

	// check to see if the user has a therapist
	for i, member := range members {
		var therapist Profile
		err := db.Conn.QueryRow(
			ctx,
			`SELECT 
				p."id",
				p."image",
				p."name"
			FROM 
				"user" p, 
				"user_therapist" pt 
			WHERE 
				p."id" = pt."therapist_id"
				AND pt."active" = true
				AND pt."user_id" = $1;`,
			member.ProfileID,
		).Scan(
			&therapist.ID,
			&therapist.Image,
			&therapist.Name,
		)
		if err != nil {
			log.WithError(err).WithField("member", member.ProfileID).Info(ctx, "error running therapist lookup query")
		} else {
			members[i].Therapist = &therapist
		}
	}

	// set the family members
	f.Members = members

	return nil
}

// NewFamily creates a new family
func NewFamily(ctx context.Context, db database.DB) (*Family, error) {
	ctx, log := logger.New(ctx, "profile.NewFamily")
	log.Info(ctx, "creating new family")

	// create a new family
	var f Family
	err := db.Conn.QueryRow(ctx, `INSERT INTO "family" ("created_at") VALUES (CURRENT_TIMESTAMP) RETURNING "id";`).
		Scan(&f.ID)
	if err != nil {
		log.WithError(err).Info(ctx, "error running family member save query")
		return nil, err
	}

	return &f, nil
}

// Delete deletes a family membership record
func (m *Membership) Delete(ctx context.Context, db database.DB) error {
	ctx, log := logger.New(
		ctx,
		"profile.Membership.Delete",
		logger.WithField("familyID", m.FamilyID),
	)

	log.Info(ctx, "deleting family membership")

	// delete the membership
	_, err := db.Conn.Exec(
		ctx,
		`DELETE FROM "family_members" WHERE "id" = $1;`,
		m.ID,
	)
	if err != nil {
		return err
	}

	return nil
}

// Save saves a family membership record
func (m *Membership) Save(ctx context.Context, db database.DB) error {
	ctx, log := logger.New(
		ctx,
		"profile.Membership.Save",
		logger.WithField("familyID", m.FamilyID),
	)

	log.Info(ctx, "saving family membership")

	// create an ID if one doesn't exist
	if m.ID == nil || *m.ID == uuid.Nil {
		id := uuid.New()
		m.ID = &id
	}

	// save the membership
	err := db.Conn.QueryRow(
		ctx,
		`INSERT INTO "family_members" ("id", "family", "user", "accepted_invite", "is_admin") VALUES ($1, $2, $3, $4, $5) ON CONFLICT ("id") DO UPDATE SET "is_admin" = $5 RETURNING "id";`,
		m.ID,
		m.FamilyID,
		m.ProfileID,
		m.AcceptedInvite,
		m.IsAdmin,
	).Scan(&m.ID)
	if err != nil {
		log.WithError(err).Info(ctx, "error running family member save query")
		return err
	}

	return nil
}
