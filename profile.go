package profile

import (
	"context"
	_ "embed"
	"time"

	"github.com/google/uuid"
	"gitlab.com/tin-roof/meerchat-aac/board"
	"gitlab.com/tin-roof/meerchat-aac/database"
	"gitlab.com/tin-roof/meerchat-aac/logger"
)

// queries
var (
	//go:embed queries/family_create.sql
	familyCreateQuery string

	//go:embed queries/family_member_create.sql
	familyMemberCreateQuery string

	//go:embed queries/page_create.sql
	pageCreateQuery string

	//go:embed queries/settings_create.sql
	settingsCreateQuery string
)

// Profile defines the profile of a user
type Profile struct {
	CreatedAt      time.Time  `json:"createdAt,omitempty"`
	Email          string     `json:"email,omitempty"`
	ID             *uuid.UUID `json:"id,omitempty"`
	Image          string     `json:"image,omitempty"`
	Name           string     `json:"name,omitempty"`
	SubscriptionID *uuid.UUID `json:"subscriptionId,omitempty"`
	IsTherapist    bool       `json:"isTherapist,omitempty"`
	UpdatedAt      time.Time  `json:"updatedAt,omitempty"`

	// joined details
	HomeBoardID *uuid.UUID `json:"homeBoardId,omitempty"`
	IsAdmin     bool       `json:"isAdmin,omitempty"`
}

// GetDetails for a profile
func (p *Profile) GetDetails(ctx context.Context, db database.DB) (*Profile, error) {
	ctx, log := logger.New(ctx, "profile.Profile.GetDetails", logger.WithField("id", p.ID))
	log.WithField("profile", p).Info(ctx, "getting profile details")

	// @TODO: move this to a query file
	err := db.Conn.QueryRow(
		ctx,
		`SELECT u."created_at", u."email", u."name", u."subscription", u."therapist", u."updated_at", p."id" as home_board_id FROM "user" u, "page" p WHERE u."id" = $1 AND p."user" = u."id" AND p."title" = 'home';`,
		p.ID,
	).Scan(&p.CreatedAt, &p.Email, &p.Name, &p.SubscriptionID, &p.IsTherapist, &p.UpdatedAt, &p.HomeBoardID)
	if err != nil {
		log.WithError(err).Info(ctx, "error getting profile details")
		return nil, err
	}

	return p, nil
}

// GetID for a profile
func (p *Profile) GetID(ctx context.Context, db database.DB) (*Profile, error) {
	ctx, _ = logger.New(ctx, "profile.Profile.GetID", logger.WithField("email", p.Email))

	err := db.Conn.QueryRow(
		ctx,
		`SELECT "id" FROM "user" WHERE "email" = $1;`,
		p.Email,
	).Scan(&p.ID)
	if err != nil {
		return p, err
	}

	return p, nil
}

// New creates all the rest of the profile
func (p *Profile) New(ctx context.Context, db database.DB) (*Profile, error) {
	// start a transaction
	if err := db.Transaction(ctx); err != nil {
		return nil, err
	}
	// roll back the transaction if there is an error
	defer db.TX.Rollback(ctx)

	// create the new family
	var familyID uuid.UUID
	if err := db.TX.Conn.QueryRow(ctx, familyCreateQuery).Scan(&familyID); err != nil {
		return nil, err
	}

	// create the family member link
	if _, err := db.TX.Conn.Exec(ctx, familyMemberCreateQuery, familyID, p.ID, false, true); err != nil {
		return nil, err
	}

	// create the default app settings
	if _, err := db.TX.Conn.Exec(ctx, settingsCreateQuery, p.ID); err != nil {
		return nil, err
	}

	// create the default page
	if _, err := db.TX.Conn.Exec(ctx, pageCreateQuery, p.ID, board.DefaultBoardButtons); err != nil {
		return nil, err
	}

	// save all the changes
	if err := db.TX.Commit(ctx); err != nil {
		return nil, err
	}

	return p, nil
}

// Save a profile
// @TODO: save should be reworked in a way that can be used for both new and existing profiles with and without a family
// - maybe the issue is actually in registration and not here ie: save the profile then create the family/membership
func (p *Profile) Save(ctx context.Context, db database.DB, isNew bool) (*Profile, error) {
	ctx, _ = logger.New(ctx, "profile.Profile.Save", logger.WithField("email", p.Email))

	// create an ID if one doesn't exist
	if p.ID == nil || *p.ID == uuid.Nil {
		id := uuid.New()
		p.ID = &id
	}

	// save the profile
	err := db.Conn.QueryRow(
		ctx,
		`INSERT INTO "user" ("id", "email", "subscription", "name", "image") VALUES ($1, $2, $3, $4, $5) ON CONFLICT ("id") DO UPDATE SET "email" = $2, "subscription" = $3, "name" = $4, "image" = $5 RETURNING "id";`,
		p.ID,
		p.Email,
		p.SubscriptionID,
		p.Name,
		p.Image,
	).Scan(&p.ID)
	if err != nil {
		return p, err
	}

	// make sure to create all the rest of the user defaults
	if isNew {
		p, err := p.New(ctx, db)
		if err != nil {
			return p, err
		}
	}

	return p, nil
}

// Create creates a new profile with some default data
func (p *Profile) Create(ctx context.Context, db database.DB) (*Profile, error) {
	ctx, log := logger.New(ctx, "profile.Profile.Create", logger.WithField("email", p.Email))

	log.Info(ctx, "creating profile")

	// start a transaction
	if err := db.Transaction(ctx); err != nil {
		return nil, err
	}
	// roll back the transaction if there is an error
	defer db.TX.Rollback(ctx)

	// create a new profile
	err := db.TX.Conn.QueryRow(
		ctx,
		`INSERT INTO "user" ("email", "name", "subscription") VALUES ($1, $2, $3) RETURNING "id";`,
		p.Email,
		p.Name,
		p.SubscriptionID,
	).Scan(&p.ID)
	if err != nil {
		log.WithError(err).Info(ctx, "failed to create user")
		return p, err
	}

	// create the default app settings
	if _, err := db.TX.Conn.Exec(ctx, settingsCreateQuery, p.ID); err != nil {
		log.WithError(err).Info(ctx, "failed to create user settings")
		return nil, err
	}

	// create the default page
	if _, err := db.TX.Conn.Exec(ctx, pageCreateQuery, p.ID, board.DefaultBoardButtons); err != nil {
		log.WithError(err).Info(ctx, "failed to create default home page")
		return nil, err
	}

	// save all the changes
	if err := db.TX.Commit(ctx); err != nil {
		return nil, err
	}

	return p, nil
}

// Delete removes a profile
func (p *Profile) Delete(ctx context.Context, db database.DB) error {
	ctx, log := logger.New(ctx, "profile.Profile.Delete", logger.WithField("email", p.Email))

	log.Info(ctx, "deleting profile")

	// delete the user
	if _, err := db.Conn.Exec(ctx, `DELETE FROM "user" WHERE "id" = $1;`, p.ID); err != nil {
		log.WithError(err).Info(ctx, "failed to delete user")
		return err
	}

	return nil
}
