package profile

import (
	"context"
	"testing"
	"time"

	"github.com/google/uuid"
)

// TestValidateToken tests the ValidateToken function
func TestValidateToken(t *testing.T) {
	secret := "testSecret"
	id := uuid.New()
	ctx := context.Background()
	oldProfile := Profile{
		ID: &id,
	}

	tokenString, _ := oldProfile.Tokenize(ctx, secret, time.Now())

	newProfile := &Profile{}
	newProfile, err := newProfile.ValidateToken(ctx, secret, tokenString)
	if err != nil {
		t.Error("error with the validation")
	}
	if *newProfile.ID != *oldProfile.ID {
		t.Error("profile IDs do not match")
	}

}
