package profile

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"time"

	jwt "github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
	"gitlab.com/tin-roof/meerchat-aac/logger"
)

type claims struct {
	Email string `json:"email"`
	Name  string `json:"name"`
	jwt.RegisteredClaims
}

// Tokenize creates a JWT token for the profile
func (p *Profile) Tokenize(ctx context.Context, secret string, timestamp time.Time) (string, error) {
	ctx, log := logger.New(ctx, "profile.Profile.GetJWT", logger.WithField("email", p.Email))
	log.Info(ctx, "creating auth token")

	// create the claims
	c := claims{
		Email: p.Email,
		Name:  p.Name,
		RegisteredClaims: jwt.RegisteredClaims{
			Issuer: "meerchat",
			// ExpiresAt: jwt.NewNumericDate(timestamp.Add(24 * time.Hour)), // @TODO: set this once token refreshing becomes a thing with the rest of the app
			IssuedAt: jwt.NewNumericDate(timestamp),
		},
	}

	// set claim id if the profile has one and its not empty
	if p.ID != nil && *p.ID != uuid.Nil {
		c.RegisteredClaims.ID = p.ID.String()
	}

	// create the token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, c)

	tokenString, err := token.SignedString([]byte(secret))
	if err != nil {
		log.WithError(err).Info(ctx, "error signing token")
		return "", err
	}

	return tokenString, nil
}

// ValidateToken checks the JWT token for the profile to make sure its valid
func (p *Profile) ValidateToken(ctx context.Context, secret string, tokenString string) (*Profile, error) {
	ctx, log := logger.New(ctx, "profile.Profile.Validate")
	log.WithField("tokenString", tokenString).Info(ctx, "validating auth token")

	// remove the bearer from the token
	tokenString = strings.TrimPrefix(tokenString, "Bearer ")

	// parse the token
	var c claims
	token, err := jwt.ParseWithClaims(tokenString, &c, func(token *jwt.Token) (interface{}, error) {
		// don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		return []byte(secret), nil
	})

	if err != nil {
		log.WithError(err).Info(ctx, "error parsing token")
		return p, err
	}

	// handle invalid token
	if !token.Valid {
		log.WithError(err).Info(ctx, "error looking at claims")
		return p, errors.New("error validating token")
	}

	// @TODO: make sure the token is not expired once token refreshing becomes a thing with the rest of the app

	// parse the id to make sure its valid if its in the token
	if c.RegisteredClaims.ID != "" {
		id, err := uuid.Parse(c.RegisteredClaims.ID)
		if err != nil {
			log.WithError(err).Info(ctx, "error parsing user id")
			return p, err
		}

		p.ID = &id
	}

	// set the id
	p.Email = c.Email
	p.Name = c.Name

	log.Info(ctx, "success auth token")
	return p, nil
}
