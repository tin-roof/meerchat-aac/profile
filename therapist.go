package profile

import (
	"context"
	"errors"
	"time"

	"github.com/google/uuid"
	"gitlab.com/tin-roof/meerchat-aac/database"
	"gitlab.com/tin-roof/meerchat-aac/logger"
)

// Client is a therapist's client
type Client struct {
	Active      bool       `db:"active" json:"active"`
	CreatedAt   *time.Time `db:"created_at" json:"createdAt,omitempty"`
	TherapistID *uuid.UUID `db:"therapist_id" json:"therapistId,omitempty"`
	ID          *uuid.UUID `db:"id" json:"id,omitempty"`
	ProfileID   *uuid.UUID `db:"user_id" json:"profileId,omitempty"`
	UpdatedAt   *time.Time `db:"updated_at" json:"updatedAt,omitempty"`

	// joined fields
	Profile *Profile `json:"profile,omitempty"`
}

// deleteTherapistLinks deletes any therapist links for the profile
func (p *Profile) DeleteTherapistLinks(ctx context.Context, db database.DB) error {
	ctx, log := logger.New(ctx, "profile.Profile.deleteTherapistLinks", logger.WithField("id", p.ID))
	log.Info(ctx, "deleting therapist links for profile")

	// delete the therapist links
	// @TODO: this should update the therapist link to be inactive instead of deleting it
	// so that we don't loose the history
	_, err := db.Conn.Exec(
		ctx,
		`DELETE FROM "user_therapist" WHERE "user_id" = $1;`,
		p.ID,
	)
	if err != nil {
		log.Info(ctx, "error deleting therapist links for profile")
		return err
	}

	return nil
}

// GetClientList finds all the patients linked to a therapist
func (p *Profile) GetClientList(ctx context.Context, db database.DB) ([]Client, error) {
	ctx, log := logger.New(ctx, "profile.Profile.GetClientList", logger.WithField("id", p.ID))

	// look up the user if it hasn't been already
	if p.CreatedAt.IsZero() {
		if _, err := p.GetDetails(ctx, db); err != nil {
			return nil, err
		}
	}

	// skip function if the user is not a therapist
	if !p.IsTherapist {
		return nil, errors.New("user is not a therapist")
	}

	// lookup the family members
	rows, err := db.Conn.Query(
		ctx,
		`SELECT 
			ut."id",
			ut."created_at",
			ut."updated_at",
			ut."therapist_id",
			ut."user_id" as profile_id,
			ut."active",
    		u."created_at" as profile_created_at, 
    		u."subscription" as profile_subscription_id,
    		u."email" as profile_email,
    		u."name" as profile_name,
			u."image" as profile_image,
    		p."id" as home_board_id
		FROM 
    		"user" u, 
    		"user_therapist" ut,
    		"page" p
		WHERE 
    		u."id" = ut."user_id"
    		AND p."user" = ut."user_id"
    		AND p."title" = 'home'
    		AND ut."active" = true
    		AND ut."therapist_id" = $1;`,
		p.ID,
	)
	if err != nil {
		log.WithError(err).Info(ctx, "error running therapist patient lookup query")
		return nil, err
	}

	// build the page list
	clients := []Client{}
	for rows.Next() {
		client := Client{Profile: &Profile{}}
		if err := rows.Scan(
			&client.ID,
			&client.CreatedAt,
			&client.UpdatedAt,
			&client.TherapistID,
			&client.ProfileID,
			&client.Active,
			&client.Profile.CreatedAt,
			&client.Profile.SubscriptionID,
			&client.Profile.Email,
			&client.Profile.Name,
			&client.Profile.Image,
			&client.Profile.HomeBoardID,
		); err != nil {
			log.WithError(err).Info(ctx, "error scanning therapist client list query row")
			continue
		}

		clients = append(clients, client)
	}

	return clients, nil
}

// LinkTherapist links a therapist to a profile
func (p *Profile) LinkTherapist(ctx context.Context, db database.DB, therapist *Profile) (*Profile, error) {
	ctx, log := logger.New(ctx, "profile.Profile.LinkTherapist", logger.WithField("email", p.Email))
	log.Info(ctx, "linking therapist")

	// make sure the therapist is valid
	if therapist == nil {
		log.Info(ctx, "no therapist provided")
		return nil, errors.New("no therapist provided")
	}

	// make sure the therapist has an id
	if therapist.ID == nil || *therapist.ID == uuid.Nil {
		log.Info(ctx, "therapist id is nil")
		return nil, errors.New("therapist id is nil")
	}

	// lookup the therapist details to verify that the profile is indeed a therapist
	if _, err := therapist.GetDetails(ctx, db); err != nil {
		log.WithError(err).Info(ctx, "failed to lookup therapist")
		return nil, err
	}

	if !therapist.IsTherapist {
		log.Info(ctx, "therapist id is not a valid therapist")
		return nil, errors.New("therapist id is not a valid therapist")
	}

	// mark all current therapists as inactive
	_, err := db.Conn.Exec(
		ctx,
		`UPDATE "user_therapist" SET "active" = false WHERE "therapist_id" = $1 AND "user_id" = $2 AND "active" = true;`,
		therapist.ID,
		p.ID,
	)
	if err != nil {
		log.WithError(err).Info(ctx, "failed to link therapist")
		return nil, err
	}

	// create the therapist link to the profile or activate and existing link
	_, err = db.Conn.Exec(
		ctx,
		`INSERT INTO 
			"user_therapist" ("therapist_id", "user_id") VALUES($1, $2) 
		ON CONFLICT ("therapist_id", "user_id") DO 
		UPDATE SET "active" = true RETURNING "id";`,
		therapist.ID,
		p.ID,
	)
	if err != nil {
		log.WithError(err).Info(ctx, "failed to link therapist")
		return nil, err
	}

	return p, nil
}
