package profile

import (
	"context"

	"gitlab.com/tin-roof/meerchat-aac/database"
	"gitlab.com/tin-roof/meerchat-aac/logger"
)

// DeleteAllBoards deletes the users boards
func (p *Profile) DeleteAllBoards(ctx context.Context, db database.DB) error {
	ctx, log := logger.New(ctx, "profile.Profile.DeleteBoards", logger.WithField("profileID", p.ID))
	log.Info(ctx, "deleting boards for profile")

	// mark any published boards as deleted, we don't want to delete them because they may be used by other people
	_, err := db.Conn.Exec(
		ctx,
		`UPDATE "page" SET "user" = NULL, "deleted" = TRUE WHERE "user" = $1 AND "published" = TRUE;`,
		p.ID,
	)
	if err != nil {
		log.Info(ctx, "error deleting boards for profile")
		return err
	}

	// fully delete any unpublished boards, no way other people are using them so they are safe to remove
	_, err = db.Conn.Exec(
		ctx,
		`DELETE FROM "page" WHERE "user" = $1 AND "published" = FALSE;`,
		p.ID,
	)
	if err != nil {
		log.Info(ctx, "error deleting boards for profile")
		return err
	}

	return nil
}
